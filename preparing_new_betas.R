# ==== preparing the new betas ====

library( ff )
library( tidyr )
library( plyr )
library( dplyr )

my.wd <- getwd()
setwd( "../Haplin_metylering/Haplin/R/" )
all.funcs <- list.files()
invisible( sapply( all.funcs, source ) )
setwd( my.wd )

.onLoad( "Haplin", "Haplin" )

source( "common_variables.R" )

# ---- 1. read_betas ----
# 1. reading new betas
betas.ff <- envDataLoad( "newest_betas" )
betas.ff <- betas.ff[[1]]
dim( betas.ff )

# since the new betas have different indiv.ids, it's best to translate it back to the old scheme
link.old.new <- read.table( "../ORIG_DATA/idlink.csv", sep = ",", header = TRUE )
link.old.new$sampleID <- sub( "@", ".", x = link.old.new$sampleID, fixed = TRUE )
head( link.old.new )

head( colnames( betas.ff ) )
colnames( betas.ff ) <- link.old.new$sampleID[ match( colnames( betas.ff ), link.old.new$id ) ]
head( colnames( betas.ff ) )

# ---- 2. snp_info ----
# 2. gathering info about the SNPs
cpg.info <- read.csv( "../ORIG_DATA/cpg_info_all.csv" )

cpg.info$TargetID <- as.character( cpg.info$TargetID )
rownames( cpg.info ) <- cpg.info$TargetID
cpg.info <- cpg.info[ !is.na(cpg.info$MAPINFO), ]

load( "reg_info_found_obj.RData" )

my.SNP.map
str( my.CpGs )

# ---- 4. subset_betas ----
# 4. choosing a subset of betas
which.rows.betas <- sapply( as.character( my.SNP.map$gene_name ), function(x){
	cur.cpgs <- my.CpGs[[ x ]]
	match( as.character( cur.cpgs$id ), rownames( betas.ff ) )
} )
names( which.rows.betas ) <- as.character( my.SNP.map$gene_name )
sapply( which.rows.betas, length )

my.CpGs.betas <- lapply( as.character( my.SNP.map$gene_name ), function(x){
	cur.rows <- which.rows.betas[[ x ]]
	cur.which.na <- is.na( cur.rows )
	cur.rows <- cur.rows[ !cur.which.na ]
	if( !is.null( cur.rows ) ){
		tmp.betas <- betas.ff[ cur.rows, , drop = FALSE ]
		data.frame( CpG = rownames( tmp.betas ), position = my.CpGs[[ x ]]$coord[ !cur.which.na ],
			tmp.betas )
	}
} )
names( my.CpGs.betas ) <- as.character( my.SNP.map$gene_name )

sapply( my.CpGs.betas, dim )
my.CpGs.betas[[ 1 ]][ 1:3, 1:10 ]

# ---- 5. M-values ----
# 5. create M values from the beta values
m <- function( beta ){
	log2( beta/( 1 - beta ) )
}

betas.chosen.CpGs.list <- lapply( as.character( my.SNP.map$gene_name ), function(x){
	cur.betas <- my.CpGs.betas[[ x ]]
	if( !is.null( cur.betas ) ){
		tmp.df <- gather( cur.betas, indiv, beta, starts_with( "BAE" ) )
		tmp.df <- arrange( tmp.df, CpG, indiv )
		mvals.tmp <- m( tmp.df$beta )
		cbind( name = x, tmp.df, mvalue = mvals.tmp )
	}
} )
names( betas.chosen.CpGs.list ) <- as.character( my.SNP.map$gene_name )
head( betas.chosen.CpGs.list[[1]] )

lapply( betas.chosen.CpGs.list, function(x){
	 summary( x$mvalue )	
} )

betas.chosen.CpGs.df <- do.call( what = "rbind", betas.chosen.CpGs.list )
which.null <- unlist( lapply( betas.chosen.CpGs.list, is.null ) )
which.null

gene.labels <- paste( as.character( my.SNP.map$gene_name[ !which.null ] ), "( chr",
		my.SNP.map$chr[ !which.null ], ")" )
names( gene.labels ) <- as.character( my.SNP.map$gene_name[ !which.null ] )
gene.labels

# ---- 6. indiv_methyl_genetic_data ----
# 6. choosing the individuals with genetic data also
all.indiv <- betas.chosen.CpGs.df$indiv
all.indiv <- t( sapply( all.indiv, function( x ){ unlist( strsplit( x, split = ".",
    fixed = TRUE ) ) } ) )
rownames( all.indiv ) <- NULL
head( all.indiv )
betas.chosen.CpGs.df <- data.frame( betas.chosen.CpGs.df[ ,c( "name", "CpG", "position" ) ],
    indiv = all.indiv[ ,1 ], indiv.ctd = all.indiv[ ,2 ],
    betas.chosen.CpGs.df[ ,c( "beta", "mvalue" ) ], stringsAsFactors = FALSE )
head( betas.chosen.CpGs.df )

all.indiv <- betas.chosen.CpGs.df$indiv

# ---- 6a. matching individuals ----
# 6a. matching individuals - separately for each cleft type
betas.df.subgroup <- lapply( cleft.types, function(x){
  ids.final <- read.table( file.names[ x ], header = TRUE, stringsAsFactors = FALSE )
  cat( "cleft type: ", x, "\n")
  print( dim( ids.final ) )
  
  betas.df.tmp <- c()
  for( p in ids.final$ID.BSI ){
    betas.df.tmp <- rbind( betas.df.tmp, betas.chosen.CpGs.df[
      betas.chosen.CpGs.df$indiv == p, ] )
  }
  return( betas.df.tmp)
} )
names( betas.df.subgroup ) <- cleft.types
sapply( betas.df.subgroup, dim )
# checking whether I took correct data
sapply( betas.df.subgroup, function(x){
  length( unique( x$indiv ) )
} )

# ---- 7. saving ----
# 7. saving the data
save( betas.df.subgroup, my.SNP.map, gene.labels,	file = "newest_betas_prepared.RData" )

