# ==== QQ plots ====
library( SuppDists )
library( MASS )
library( Haplin )
library( ggplot2 )
library( ggrepel )

source( "gg_qqplot.R" )

# ==== CL dataset ====
# CL dataset
# ==== GxM ====
# GxM
load( "../GxM_PoOxM_new_betas_mult_CLO.RData" )

gxe.pval.promoter.cl <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} ) )

gxe.pval.enhancer.cl <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

gxe.pval.gene.cl <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.cl ),
		rownames( gxe.pval.enhancer.cl ), rownames( gxe.pval.gene.cl ) ),
			my.SNP.map$gene_name ) ] )

gxe.pval.all.cl <- data.frame( region = c( rep( "promoter", nrow( gxe.pval.promoter.cl ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.cl ) ), rep( "gene", nrow( gxe.pval.gene.cl ) ) ),
		gene.name = c( rownames( gxe.pval.promoter.cl ), rownames( gxe.pval.enhancer.cl ),
		  rownames( gxe.pval.gene.cl ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( gxe.pval.promoter.cl[ , "pval" ], gxe.pval.enhancer.cl[ , "pval" ],
		  gxe.pval.gene.cl[ , "pval" ] ) ),
		df = unlist( c( gxe.pval.promoter.cl[ , "df" ], gxe.pval.enhancer.cl[ , "df" ],
		  gxe.pval.gene.cl[ , "df" ] ) ) )
head( gxe.pval.all.cl )
dim( gxe.pval.all.cl )

df.all.cl <- unique( gxe.pval.all.cl$df )
df.all.cl

# plotting
pvals.GxM.CL <- gxe.pval.all.cl$pval
names( pvals.GxM.CL ) <- paste( gxe.pval.all.cl$region, gxe.pval.all.cl$snp.name,
  sep = "_" )
pvals.GxM.CL
cur.len <- length( pvals.GxM.CL )
cur.nlabs <- 2

cur.xcoords <- get_x_coords( length( pvals.GxM.CL ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.CL ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.CL ) +
  labs( title = "GxMe, CLO dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "GxM_CL_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== POOxM ====
# now, PoOxM for CLO
pooxm.pval.promoter <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} ) )

pooxm.pval.enhancer <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

pooxm.pval.gene <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$gene_name ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.CL <- pooxm.pval.all$pval
names( pvals.POOxM.CL ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.CL
cur.len <- length( pvals.POOxM.CL )
cur.nlabs <- 8

cur.xcoords <- get_x_coords( length( pvals.POOxM.CL ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.CL ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.CL ) +
  labs( title = "PoOxMe, CLO dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ), xlim = c( 1,NA ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_CL_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )


# ==== CL/P dataset ====
load( "../GxM_PoOxM_new_betas_mult_CLorP.RData" )

# ==== GxM ====
# GxM
gxe.pval.promoter.clorp <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} ) )

gxe.pval.enhancer.clorp <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

gxe.pval.gene.clorp <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.clorp ),
		rownames( gxe.pval.enhancer.clorp ), rownames( gxe.pval.gene.clorp ) ),
			my.SNP.map$gene_name ) ] )

gxe.pval.all.clorp <- data.frame( region = c( rep( "promoter", nrow( gxe.pval.promoter.clorp ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.clorp ) ), rep( "gene", nrow( gxe.pval.gene.clorp ) ) ),
		gene.name = c( rownames( gxe.pval.promoter.clorp ), rownames( gxe.pval.enhancer.clorp ),
		  rownames( gxe.pval.gene.clorp ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( gxe.pval.promoter.clorp[ , "pval" ], gxe.pval.enhancer.clorp[ , "pval" ],
		  gxe.pval.gene.clorp[ , "pval" ] ) ),
		df = unlist( c( gxe.pval.promoter.clorp[ , "df" ], gxe.pval.enhancer.clorp[ , "df" ],
		  gxe.pval.gene.clorp[ , "df" ] ) ) )
head( gxe.pval.all.clorp )
dim( gxe.pval.all.clorp )

df.all.clorp <- unique( gxe.pval.all.clorp$df )
df.all.clorp

# plotting
pvals.GxM.clorp <- gxe.pval.all.clorp$pval
names( pvals.GxM.clorp ) <- paste( gxe.pval.all.clorp$region, gxe.pval.all.clorp$snp.name,
  sep = "_" )
pvals.GxM.clorp
cur.len <- length( pvals.GxM.clorp )
cur.nlabs <- 2

cur.xcoords <- get_x_coords( length( pvals.GxM.clorp ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.clorp ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.clorp ) +
  labs( title = "GxMe, CL/P dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "GxM_CLorP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== POOxM ====
# now, PoOxM for CL/P
pooxm.pval.promoter <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} ) )

pooxm.pval.enhancer <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

pooxm.pval.gene <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$gene_name ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.clorp <- pooxm.pval.all$pval
names( pvals.POOxM.clorp ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.clorp
cur.len <- length( pvals.POOxM.clorp )
cur.nlabs <- 8

cur.xcoords <- get_x_coords( length( pvals.POOxM.clorp ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.clorp ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.clorp ) +
  labs( title = "PoOxMe, CL/P dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ), #xlim = c( 1.5,NA ),
                    force = 10, nudge_y = cur.labels$x - 1, nudge_x = 2 - cur.labels$x ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_CLorP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )



# ==== CLP dataset ====
load( "../GxM_PoOxM_new_betas_mult_CLP.RData" )

# ==== GxM ====
# GxM
gxe.pval.promoter.clp <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} ) )

gxe.pval.enhancer.clp <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

gxe.pval.gene.clp <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.clp ),
		rownames( gxe.pval.enhancer.clp ), rownames( gxe.pval.gene.clp ) ),
			my.SNP.map$gene_name ) ] )

gxe.pval.all.clp <- data.frame( region = c( rep( "promoter", nrow( gxe.pval.promoter.clp ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.clp ) ), rep( "gene", nrow( gxe.pval.gene.clp ) ) ),
		gene.name = c( rownames( gxe.pval.promoter.clp ), rownames( gxe.pval.enhancer.clp ),
		  rownames( gxe.pval.gene.clp ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( gxe.pval.promoter.clp[ , "pval" ], gxe.pval.enhancer.clp[ , "pval" ],
		  gxe.pval.gene.clp[ , "pval" ] ) ),
		df = unlist( c( gxe.pval.promoter.clp[ , "df" ], gxe.pval.enhancer.clp[ , "df" ],
		  gxe.pval.gene.clp[ , "df" ] ) ) )
head( gxe.pval.all.clp )
dim( gxe.pval.all.clp )

df.all.clp <- unique( gxe.pval.all.clp$df )
df.all.clp

# plotting
pvals.GxM.clp <- gxe.pval.all.clp$pval
names( pvals.GxM.clp ) <- paste( gxe.pval.all.clp$region, gxe.pval.all.clp$snp.name,
  sep = "_" )
pvals.GxM.clp
cur.len <- length( pvals.GxM.clp )
cur.nlabs <- 2

cur.xcoords <- get_x_coords( length( pvals.GxM.clp ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.clp ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.clp ) +
  labs( title = "GxMe, CLP dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "GxM_CLP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== POOxM ====
# now, PoOxM for CLP
pooxm.pval.promoter <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} ) )

pooxm.pval.enhancer <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

pooxm.pval.gene <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$gene_name ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.clp <- pooxm.pval.all$pval
names( pvals.POOxM.clp ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.clp
cur.len <- length( pvals.POOxM.clp )
cur.nlabs <- 8

cur.xcoords <- get_x_coords( length( pvals.POOxM.clp ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.clp ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.clp ) +
  labs( title = "PoOxMe, CLP dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ),# xlim = c( 1,NA ) ) +
                force = 10, nudge_y = cur.labels$x - 1, nudge_x = 2 - cur.labels$x ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_CLP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== CPO dataset ====
load( "../GxM_PoOxM_new_betas_mult_CPO.RData" )

# ==== GxM ====
# GxM
gxe.pval.promoter.cpo <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} ) )

gxe.pval.enhancer.cpo <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

gxe.pval.gene.cpo <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.cpo ),
		rownames( gxe.pval.enhancer.cpo ), rownames( gxe.pval.gene.cpo ) ),
			my.SNP.map$gene_name ) ] )

gxe.pval.all.cpo <- data.frame( region = c( rep( "promoter", nrow( gxe.pval.promoter.cpo ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.cpo ) ), rep( "gene", nrow( gxe.pval.gene.cpo ) ) ),
		gene.name = c( rownames( gxe.pval.promoter.cpo ), rownames( gxe.pval.enhancer.cpo ),
		  rownames( gxe.pval.gene.cpo ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( gxe.pval.promoter.cpo[ , "pval" ], gxe.pval.enhancer.cpo[ , "pval" ],
		  gxe.pval.gene.cpo[ , "pval" ] ) ),
		df = unlist( c( gxe.pval.promoter.cpo[ , "df" ], gxe.pval.enhancer.cpo[ , "df" ],
		  gxe.pval.gene.cpo[ , "df" ] ) ) )
head( gxe.pval.all.cpo )
dim( gxe.pval.all.cpo )

df.all.cpo <- unique( gxe.pval.all.cpo$df )
df.all.cpo

# plotting
pvals.GxM.cpo <- gxe.pval.all.cpo$pval
names( pvals.GxM.cpo ) <- paste( gxe.pval.all.cpo$region, gxe.pval.all.cpo$snp.name,
  sep = "_" )
pvals.GxM.cpo
cur.len <- length( pvals.GxM.cpo )
cur.nlabs <- 2

cur.xcoords <- get_x_coords( length( pvals.GxM.cpo ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.cpo ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.cpo ) +
  labs( title = "GxMe, CPO dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "GxM_CPO_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== POOxM ====
# now, PoOxM for CPO
pooxm.pval.promoter <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} ) )

pooxm.pval.enhancer <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

pooxm.pval.gene <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$gene_name ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.cpo <- pooxm.pval.all$pval
names( pvals.POOxM.cpo ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.cpo
cur.len <- length( pvals.POOxM.cpo )
cur.nlabs <- 8

cur.xcoords <- get_x_coords( length( pvals.POOxM.cpo ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.cpo ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.cpo ) +
  labs( title = "PoOxMe, CPO dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ), #xlim = c( 1,NA ) ) +
                force = 10, nudge_y = cur.labels$x - 1, nudge_x = 1.5 - cur.labels$x ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_CPO_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== control dataset ====
load( "../GxM_PoOxM_new_betas_mult_cntrl.RData" )

# ==== GxM ====
# GxM

gxe.pval.promoter.cntrl <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} ) )

gxe.pval.enhancer.cntrl <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

gxe.pval.gene.cntrl <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.cntrl ),
		rownames( gxe.pval.enhancer.cntrl ), rownames( gxe.pval.gene.cntrl ) ),
			my.SNP.map$gene_name ) ] )

gxe.pval.all.cntrl <- data.frame( region = c( rep( "promoter", nrow( gxe.pval.promoter.cntrl ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.cntrl ) ), rep( "gene", nrow( gxe.pval.gene.cntrl ) ) ),
		gene.name = c( rownames( gxe.pval.promoter.cntrl ), rownames( gxe.pval.enhancer.cntrl ),
		  rownames( gxe.pval.gene.cntrl ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( gxe.pval.promoter.cntrl[ , "pval" ], gxe.pval.enhancer.cntrl[ , "pval" ],
		  gxe.pval.gene.cntrl[ , "pval" ] ) ),
		df = unlist( c( gxe.pval.promoter.cntrl[ , "df" ], gxe.pval.enhancer.cntrl[ , "df" ],
		  gxe.pval.gene.cntrl[ , "df" ] ) ) )
head( gxe.pval.all.cntrl )
dim( gxe.pval.all.cntrl )

df.all.cntrl <- unique( gxe.pval.all.cntrl$df )
df.all.cntrl

# plotting
pvals.GxM.cntrl <- gxe.pval.all.cntrl$pval
names( pvals.GxM.cntrl ) <- paste( gxe.pval.all.cntrl$region, gxe.pval.all.cntrl$snp.name,
  sep = "_" )
pvals.GxM.cntrl
cur.len <- length( pvals.GxM.cntrl )
cur.nlabs <- 2

cur.xcoords <- get_x_coords( length( pvals.GxM.cntrl ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.cntrl ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.cntrl ) +
  labs( title = "GxMe, control dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "GxM_cntrl_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== POOxM ====
# now, PoOxM for cntrl
pooxm.pval.promoter <- t( sapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} ) )

pooxm.pval.enhancer <- t( sapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

pooxm.pval.gene <- t( sapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} ) )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$gene_name ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.cntrl <- pooxm.pval.all$pval
names( pvals.POOxM.cntrl ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.cntrl
cur.len <- length( pvals.POOxM.cntrl )
cur.nlabs <- 8

cur.xcoords <- get_x_coords( length( pvals.POOxM.cntrl ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.cntrl ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.cntrl ) +
  labs( title = "PoOxMe, control dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ),# xlim = c( 1,NA ) ) +
                force = 10, nudge_y = cur.labels$x - 1, nudge_x = 2 - cur.labels$x ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_cntrl_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== RANDOM SNPS, on CL/P ====

load( "../RandomSNPs/GxM_PoOxM_new_betas_mult_randomSNPs_CLorP.RData" )

# ==== GxM ====
# GxM

gxe.pval.promoter.random <- lapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} )
gxe.pval.promoter.random <- do.call( rbind, gxe.pval.promoter.random )

gxe.pval.enhancer.random <- lapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} )
gxe.pval.enhancer.random <- do.call( rbind, gxe.pval.enhancer.random )

gxe.pval.gene.random <- lapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} )
gxe.pval.gene.random <- do.call( rbind, gxe.pval.gene.random )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.random ),
		rownames( gxe.pval.enhancer.random ), rownames( gxe.pval.gene.random ) ),
			my.SNP.map$Marker ) ] )

gxe.pval.all.random <- data.frame( region = c( rep( "promoter", nrow( gxe.pval.promoter.random ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.random ) ), rep( "gene", nrow( gxe.pval.gene.random ) ) ),
		gene.name = c( rownames( gxe.pval.promoter.random ), rownames( gxe.pval.enhancer.random ),
		  rownames( gxe.pval.gene.random ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( gxe.pval.promoter.random[ , "pval" ], gxe.pval.enhancer.random[ , "pval" ],
		  gxe.pval.gene.random[ , "pval" ] ) ),
		df = unlist( c( gxe.pval.promoter.random[ , "df" ], gxe.pval.enhancer.random[ , "df" ],
		  gxe.pval.gene.random[ , "df" ] ) ) )
head( gxe.pval.all.random )
dim( gxe.pval.all.random )

df.all.random <- unique( gxe.pval.all.random$df )
df.all.random

# plotting
pvals.GxM.random <- gxe.pval.all.random$pval
names( pvals.GxM.random ) <- paste( gxe.pval.all.random$region, gxe.pval.all.random$snp.name,
  sep = "_" )
pvals.GxM.random
cur.len <- length( pvals.GxM.random )
cur.nlabs <- 4

cur.xcoords <- get_x_coords( length( pvals.GxM.random ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.random ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.random ) +
  labs( title = "GxMe, random SNPs, CL/P dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "GxM_random_CLorP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== POOxM ====
# now, PoOxM for CLO
pooxm.pval.promoter <- lapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} )
pooxm.pval.promoter <- do.call( rbind, pooxm.pval.promoter )

pooxm.pval.enhancer <- lapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} )
pooxm.pval.enhancer <- do.call( rbind, pooxm.pval.enhancer )

pooxm.pval.gene <- lapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} )
pooxm.pval.gene <- do.call( rbind, pooxm.pval.gene )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$Marker ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.random <- pooxm.pval.all$pval
names( pvals.POOxM.random ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.random
cur.len <- length( pvals.POOxM.random )
cur.nlabs <- 4

cur.xcoords <- get_x_coords( length( pvals.POOxM.random ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.random ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.random ) +
  labs( title = "PoOxMe, random SNPs, CL/P dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ), xlim = c( 1,NA ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_random_CLorP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )

# ==== TOP 20 SNPs FROM PoO SCAN on CLP ====

load( "../PoO_scan/GxM_PoOxM_new_betas_mult_top-SNPs-PoO-scan_CLP.RData" )

# ==== GxM ====
# GxM

gxe.pval.promoter.scan.top20 <- lapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} )
gxe.pval.promoter.scan.top20 <- do.call( rbind, gxe.pval.promoter.scan.top20 )

gxe.pval.enhancer.scan.top20 <- lapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} )
gxe.pval.enhancer.scan.top20 <- do.call( rbind, gxe.pval.enhancer.scan.top20 )

gxe.pval.gene.scan.top20 <- lapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} )
gxe.pval.gene.scan.top20 <- do.call( rbind, gxe.pval.gene.scan.top20 )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.scan.top20 ),
		rownames( gxe.pval.enhancer.scan.top20 ), rownames( gxe.pval.gene.scan.top20 ) ),
			my.SNP.map$Marker ) ] )

gxe.pval.all.scan.top20 <- data.frame( 
  region = c( rep( "promoter", nrow( gxe.pval.promoter.scan.top20 ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.scan.top20 ) ),
		rep( "gene", nrow( gxe.pval.gene.scan.top20 ) ) ),
	gene.name = c( rownames( gxe.pval.promoter.scan.top20 ),
	   rownames( gxe.pval.enhancer.scan.top20 ),
		 rownames( gxe.pval.gene.scan.top20 ) ),
	snp.name = cur.snp.names,
	pval = unlist( c( gxe.pval.promoter.scan.top20[ , "pval" ],
	                  gxe.pval.enhancer.scan.top20[ , "pval" ],
		                gxe.pval.gene.scan.top20[ , "pval" ] ) ),
	df = unlist( c( gxe.pval.promoter.scan.top20[ , "df" ],
	                gxe.pval.enhancer.scan.top20[ , "df" ],
		              gxe.pval.gene.scan.top20[ , "df" ] ) ) )
head( gxe.pval.all.scan.top20 )
dim( gxe.pval.all.scan.top20 )

df.all.scan.top20 <- unique( gxe.pval.all.scan.top20$df )
df.all.scan.top20

# plotting
pvals.GxM.scan.top20 <- gxe.pval.all.scan.top20$pval
names( pvals.GxM.scan.top20 ) <- paste( gxe.pval.all.scan.top20$region, gxe.pval.all.scan.top20$snp.name,
  sep = "_" )
pvals.GxM.scan.top20
cur.len <- length( pvals.GxM.scan.top20 )
cur.nlabs <- 4

cur.xcoords <- get_x_coords( length( pvals.GxM.scan.top20 ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.scan.top20 ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.scan.top20 ) +
  labs( title = "GxMe, CLP dataset",
        subtitle = "top 20 SNPs from PoO scan on CLP dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )

# ==== POOxM ====
# now, PoOxM for CLP
pooxm.pval.promoter <- lapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} )
pooxm.pval.promoter <- do.call( rbind, pooxm.pval.promoter )

pooxm.pval.enhancer <- lapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} )
pooxm.pval.enhancer <- do.call( rbind, pooxm.pval.enhancer )

pooxm.pval.gene <- lapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} )
pooxm.pval.gene <- do.call( rbind, pooxm.pval.gene )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$Marker ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.scan.top20 <- pooxm.pval.all$pval
names( pvals.POOxM.scan.top20 ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.scan.top20
cur.len <- length( pvals.POOxM.scan.top20 )
cur.nlabs <- 4

cur.xcoords <- get_x_coords( length( pvals.POOxM.scan.top20 ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.scan.top20 ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.scan.top20 ) +
  labs( title = "PoOxMe, CLP dataset",
        subtitle = "top 20 SNPs from PoO scan on CLP dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ), xlim = c( 1,NA ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_top20SNPs-PoO-scan_CLP_mult_betas_gg-qqplot_square.png", width = 14, units = "cm",
        height = 14 )



# ==== GxM and PoOxM on controls, TOP 20 SNPs FROM PoO SCAN on CLP ====

load( "../PoO_scan/GxM_PoOxM_new_betas_mult_top-SNPs-PoO-scan_cntrl.RData" )

# ==== GxM ====
# GxM

gxe.pval.promoter.cntrl.scan.top20 <- lapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$gxm$pval[ 2 ], df = x$gxm$df[ 2 ] ) )
} )
gxe.pval.promoter.cntrl.scan.top20 <- do.call( rbind, gxe.pval.promoter.cntrl.scan.top20 )

gxe.pval.enhancer.cntrl.scan.top20 <- lapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} )
gxe.pval.enhancer.cntrl.scan.top20 <- do.call( rbind, gxe.pval.enhancer.cntrl.scan.top20 )

gxe.pval.gene.cntrl.scan.top20 <- lapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$gxm$gxe.test$pval[ 2 ], df = x$gxm$gxe.test$df[ 2 ] ) )
} )
gxe.pval.gene.cntrl.scan.top20 <- do.call( rbind, gxe.pval.gene.cntrl.scan.top20 )

# creating the data.frame
cur.snp.names <- as.character( my.SNP.map$Marker[ match( c( rownames( gxe.pval.promoter.cntrl.scan.top20 ),
		rownames( gxe.pval.enhancer.cntrl.scan.top20 ), rownames( gxe.pval.gene.cntrl.scan.top20 ) ),
			my.SNP.map$Marker ) ] )

gxe.pval.all.cntrl.scan.top20 <- data.frame( 
  region = c( rep( "promoter", nrow( gxe.pval.promoter.cntrl.scan.top20 ) ),
		rep( "enhancer", nrow( gxe.pval.enhancer.cntrl.scan.top20 ) ),
		rep( "gene", nrow( gxe.pval.gene.cntrl.scan.top20 ) ) ),
	gene.name = c( rownames( gxe.pval.promoter.cntrl.scan.top20 ),
	   rownames( gxe.pval.enhancer.cntrl.scan.top20 ),
		 rownames( gxe.pval.gene.cntrl.scan.top20 ) ),
	snp.name = cur.snp.names,
	pval = unlist( c( gxe.pval.promoter.cntrl.scan.top20[ , "pval" ],
	                  gxe.pval.enhancer.cntrl.scan.top20[ , "pval" ],
		                gxe.pval.gene.cntrl.scan.top20[ , "pval" ] ) ),
	df = unlist( c( gxe.pval.promoter.cntrl.scan.top20[ , "df" ],
	                gxe.pval.enhancer.cntrl.scan.top20[ , "df" ],
		              gxe.pval.gene.cntrl.scan.top20[ , "df" ] ) ) )
head( gxe.pval.all.cntrl.scan.top20 )
dim( gxe.pval.all.cntrl.scan.top20 )

df.all.cntrl.scan.top20 <- unique( gxe.pval.all.cntrl.scan.top20$df )
df.all.cntrl.scan.top20

# plotting
pvals.GxM.cntrl.scan.top20 <- gxe.pval.all.cntrl.scan.top20$pval
names( pvals.GxM.cntrl.scan.top20 ) <- paste( gxe.pval.all.cntrl.scan.top20$region, gxe.pval.all.cntrl.scan.top20$snp.name,
  sep = "_" )
pvals.GxM.cntrl.scan.top20
cur.len <- length( pvals.GxM.cntrl.scan.top20 )
cur.nlabs <- 4

cur.xcoords <- get_x_coords( length( pvals.GxM.cntrl.scan.top20 ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.GxM.cntrl.scan.top20 ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.GxM.cntrl.scan.top20 ) +
  labs( title = "GxMe, control dataset",
        subtitle = "top 20 SNPs from PoO scan on CLP dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )

# ==== POOxM ====
# now, PoOxM for controls
pooxm.pval.promoter <- lapply( gxe.prom.all, function(x){
	return( data.frame( pval = x$pooxm$pval[ 2 ], df = x$pooxm$df[ 2 ] ) )
} )
pooxm.pval.promoter <- do.call( rbind, pooxm.pval.promoter )

pooxm.pval.enhancer <- lapply( gxe.enh.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} )
pooxm.pval.enhancer <- do.call( rbind, pooxm.pval.enhancer )

pooxm.pval.gene <- lapply( gxe.gene.all, function(x){
	return( data.frame( pval = x$pooxm$gxe.test$pval[ 2 ], df = x$pooxm$gxe.test$df[ 2 ] ) )
} )
pooxm.pval.gene <- do.call( rbind, pooxm.pval.gene )

# creating the data.frame
my.SNP.map$Marker <- tolower( my.SNP.map$Marker )
cur.snp.names <- my.SNP.map$Marker[ match( c( rownames( pooxm.pval.promoter ),
		rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
			my.SNP.map$Marker ) ]

pooxm.pval.all <- data.frame( region = c( rep( "promoter", nrow( pooxm.pval.promoter ) ),
		rep( "enhancer", nrow( pooxm.pval.enhancer ) ), rep( "gene", nrow( pooxm.pval.gene ) ) ),
		gene.name = c( rownames( pooxm.pval.promoter ), rownames( pooxm.pval.enhancer ), rownames( pooxm.pval.gene ) ),
		snp.name = cur.snp.names,
		pval = unlist( c( pooxm.pval.promoter[ , "pval" ], pooxm.pval.enhancer[ , "pval" ], pooxm.pval.gene[ , "pval" ] ) ),
		df = unlist( c( pooxm.pval.promoter[ , "df" ], pooxm.pval.enhancer[ , "df" ], pooxm.pval.gene[ , "df" ] ) ) )
head( pooxm.pval.all )
dim( pooxm.pval.all )

df.all.pooxm <- unique( pooxm.pval.all$df )
df.all.pooxm

# plotting
pvals.POOxM.cntrl.scan.top20 <- pooxm.pval.all$pval
names( pvals.POOxM.cntrl.scan.top20 ) <- paste( pooxm.pval.all$region, pooxm.pval.all$snp.name, sep = "_" )
pvals.POOxM.cntrl.scan.top20
cur.len <- length( pvals.POOxM.cntrl.scan.top20 )
cur.nlabs <- 4

cur.xcoords <- get_x_coords( length( pvals.POOxM.cntrl.scan.top20 ), cur.nlabs )
cur.ycoords <- -log10( sort( pvals.POOxM.cntrl.scan.top20 ) )[ 1:cur.nlabs ]

cur.labels <- data.frame( x = cur.xcoords, y = cur.ycoords,
  label = names( cur.ycoords ) )
gg_qqplot( pvals.POOxM.cntrl.scan.top20 ) +
  labs( title = "PoOxMe, control dataset",
        subtitle = "top 20 SNPs from PoO scan on CLP dataset" ) +
  geom_label_repel( data = cur.labels, aes( x, y, label = label ), xlim = c( 1.5,NA ) ) +
  theme( axis.text = element_text( size = 12 ), axis.title = element_text( size = 14 ),
         title = element_text( size = 16 ) )
ggsave( filename = "PoOxM_top20SNPs-PoO-scan_CLP_cntrl_mult_betas_gg-qqplot_square.png",
        width = 14, units = "cm", height = 14 )

